package com.redvoznje.redvoznje;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.Spinner;


public class NisActivity extends ActionBarActivity implements AdapterView.OnItemSelectedListener {

    private enum Dan{
        RADNI, SUBOTA, NEDELJA
    }

    private Spinner spinner;
    private String[] vremena;
    ListView l1;
    private int redniBroj;
    ArrayAdapter<CharSequence> adapter; // Za izbor linije
    ArrayAdapter<String> adapter1; // Za listu vremena
    private Dan dan;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nis);
        Intent intent = getIntent();
        spinner = (Spinner) findViewById(R.id.ni_linije);
        l1 = (ListView) findViewById(R.id.ni_list);
        adapter = ArrayAdapter.createFromResource(this,
                R.array.linijeNis, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_nis, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        redniBroj = position;
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {
        redniBroj=0;
    }

    public void selectDay(View view){
        boolean checked = ((RadioButton) view).isChecked();

        switch(view.getId()) {
            case R.id.ni_radniDan:
                if (checked) {
                    dan = Dan.RADNI;
                    postavi();
                }
                break;
            case R.id.ni_subota:
                if (checked) {
                    dan = Dan.SUBOTA;
                    postavi();
                }
                break;
            case R.id.ni_nedelja:
                if(checked) {
                    dan = Dan.NEDELJA;
                    postavi();
                }
                break;
        }
    }

    public void postavi(){
        if(redniBroj==0){
            if(dan==Dan.RADNI){
                vremena = getResources().getStringArray(R.array.nl1ra);
                adapter1 = new ArrayAdapter<String>(this,
                        android.R.layout.simple_list_item_1, android.R.id.text1, vremena);
                l1.setAdapter(adapter1);
            }else if(dan==Dan.SUBOTA){
                vremena = getResources().getStringArray(R.array.nl1sa);
                adapter1 = new ArrayAdapter<String>(this,
                        android.R.layout.simple_list_item_1, android.R.id.text1, vremena);
                l1.setAdapter(adapter1);
            }else if(dan==Dan.NEDELJA){
                vremena = getResources().getStringArray(R.array.nl1na);
                adapter1 = new ArrayAdapter<String>(this,
                        android.R.layout.simple_list_item_1, android.R.id.text1, vremena);
                l1.setAdapter(adapter1);
            }
        }
    }
}
