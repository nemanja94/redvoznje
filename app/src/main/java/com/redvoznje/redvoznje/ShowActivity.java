package com.redvoznje.redvoznje;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Random;


public class ShowActivity extends ActionBarActivity implements AdapterView.OnItemSelectedListener{

    /*Eunmeracija za grad*/
    private enum Grad{
        BEOGRAD, NOVISAD,NIS
    }

    /*Enumeracija za dane*/
    private enum Dan{
        RADNI,SUBOTA,NEDELJA
    }

    private Grad grad;
    private Dan dan;
    private Spinner spinner;
    private String[] vremena;
    ListView l1;
    private int redniBroj;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show);
        Intent intent = getIntent();
        Spinner spinner = (Spinner) findViewById(R.id.linije);
        RadioGroup rg = (RadioGroup) findViewById(R.id.bg);
        l1 = (ListView) findViewById(R.id.list);
        /*Podesavanje grada*/
        grad = Grad.valueOf(intent.getStringExtra("grad"));

        /*U zavisnosti koji grad je selektovan prikazuje listu linija*/
        if(grad==Grad.NIS){
            ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                    R.array.linijeNis, android.R.layout.simple_spinner_item);
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spinner.setAdapter(adapter);
        }else if(grad==Grad.NOVISAD){
            ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                    R.array.linijeNoviSad, android.R.layout.simple_spinner_item);
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spinner.setAdapter(adapter);
        }else if(grad==Grad.BEOGRAD){
            rg.setVisibility(View.VISIBLE);

        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_show, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /*Podesava dan*/
    public void selectDay(View view){
        boolean checked = ((RadioButton) view).isChecked();

        switch(view.getId()) {
            case R.id.radniDan:
                if (checked) {
                    dan = Dan.RADNI;
                    postavi();
                }
                    break;
            case R.id.subota:
                if (checked) {
                    dan = Dan.SUBOTA;
                    postavi();
                }
                    break;
            case R.id.nedelja:
                if(checked) {
                    dan = Dan.NEDELJA;
                    postavi();
                }
                    break;
        }
    }

    /*Podesava tip*/
    public void selectType(View view){
        boolean checked = ((RadioButton) view).isChecked();
        Spinner spinner1  = (Spinner) findViewById(R.id.linije);
        switch(view.getId()) {
            case R.id.bus:
                if (checked){

                    ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                            R.array.linijeBgBus, android.R.layout.simple_spinner_item);
                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    spinner1.setAdapter(adapter);
                }
                break;
            case R.id.tramvaj:
                if (checked) {

                    ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                            R.array.linijeBgTram, android.R.layout.simple_spinner_item);
                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    spinner1.setAdapter(adapter);
                }
                break;
            case R.id.trolejbus:
                if(checked) {

                    ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                            R.array.linijeBgTrol, android.R.layout.simple_spinner_item);
                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    spinner1.setAdapter(adapter);
                }
                break;
        }
    }

    public void postavi(){
        if(redniBroj==0){
            if(dan==Dan.RADNI){
                vremena = getResources().getStringArray(R.array.nl1ra);
                ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                        android.R.layout.simple_list_item_1, android.R.id.text1, vremena);
                l1.setAdapter(adapter);
            }else if(dan==Dan.SUBOTA){
                vremena = getResources().getStringArray(R.array.nl1sa);
                ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                        android.R.layout.simple_list_item_1, android.R.id.text1, vremena);
                l1.setAdapter(adapter);
            }else if(dan==Dan.NEDELJA){
                vremena = getResources().getStringArray(R.array.nl1na);
                ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                        android.R.layout.simple_list_item_1, android.R.id.text1, vremena);
                l1.setAdapter(adapter);
            }
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        redniBroj=position;
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {
        redniBroj = 0;
    }
}
