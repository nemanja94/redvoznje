package com.redvoznje.redvoznje;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;


public class MainActivity extends ActionBarActivity implements View.OnClickListener{

    /*Dugmici*/
    private Button btnBeograd;
    private Button btnNoviSad;
    private Button btnNis;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        /*Inicijalizacija*/
        btnBeograd = (Button) findViewById(R.id.btnbg);
        btnNoviSad = (Button) findViewById(R.id.btnns);
        btnNis = (Button) findViewById(R.id.btnnis);

        /*Postavljanje onCLick eventa*/
        btnBeograd.setOnClickListener(this);
        btnNoviSad.setOnClickListener(this);
        btnNis.setOnClickListener(this);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /*
    * U zavisnosti koje je dugme kliknuto podesi promenljivu grad na odgovarajucu vrednost
    * i pozove novu aktivnost kojoj prosledi vrednost promenljive grad
    * */

    @Override
    public void onClick(View v) {
        Intent intent;
        int id = v.getId();

        switch(id){
            case R.id.btnbg:
                intent = new Intent(this, BeogradActivity.class);
                startActivity(intent);
                break;
            case R.id.btnns:
                intent = new Intent(this, NoviSadActivity.class);
                startActivity(intent);
                break;
            case R.id.btnnis:
                intent = new Intent(this, NisActivity.class);
                startActivity(intent);
                break;
        }

    }
}
